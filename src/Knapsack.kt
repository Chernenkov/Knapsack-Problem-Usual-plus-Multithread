import java.util.ArrayList

/**
 * Describes Knapsack 0/1 problem itself and provides two methods of bruteforce solving:
 * usual and multi-thread
 *
 * @param maxWeight     determines maximum weight, which can be carried in knapsack
 * @property bestItems     determines array of items as [ArrayList] of [Element]s
 * @property bestCost      determines best cost for some solution
 *
 * @author Alex Chernenkov
 */
class Knapsack(private val maxWeight: Int) {
    private var bestItems: ArrayList<Element> = ArrayList()
    private var bestCost: Int = 0

    /**
     * Calculates weight for [items]
     * @param items [ArrayList] of [Element]s, determines current list of items in knapsack
     */
    private fun calculateWeight(items: ArrayList<Element>) = items.sumBy { it.weight }

    /**
     * Calculates cost for [items]
     * @param items [ArrayList] of [Element]s, determines current list of items in knapsack
     */
    private fun calculateCost(items: ArrayList<Element>) = items.sumBy { it.cost }

    /**
     * Compares [items] to [bestItems].
     * If current [items]' weight is smaller then current [maxWeight]
     * and cost lower then current [bestCost], [items] is written to [bestItems]
     * and [bestCost] is recalculated
     * @param items [ArrayList] of [Element]s, determines current list of items in knapsack
     */
    private fun compareToBestList(items: ArrayList<Element>){
        if((calculateWeight(items) <= maxWeight && calculateCost(items) > bestCost)){
            bestItems = items
            bestCost = calculateCost(items)
        }
    }

    /**
     * Recursive.
     * Firstly comparing [items] to current [bestItems] through method [compareToBestList].
     * Then for each new [items] with one removed item we recursively call [chooseBestCombo].
     * Exit condition is when [items]' size is 0.
     * @param items [ArrayList] of [Element]s, determines current list of items in knapsack
     */
    fun chooseBestCombo(items: ArrayList<Element>){
        if(items.size > 0) compareToBestList(items)
        for (i in 0 until items.size) {
            val newList = ArrayList<Element>(items)
            newList.removeAt(i)
            chooseBestCombo(newList)
        }
    }

    /**
     * Multi-thread version of [chooseBestCombo].
     * Here each thread is given a part of recursive work, when one element is removed from the list
     * @param items         [ArrayList] of [Element]s, determines current list of items in knapsack
     * @param threadCount   describes number of threads to solve the problem
     */
    @Throws(Exception::class)
    fun threadChooseBestCombo(items: ArrayList<Element>, threadCount: Int){
        bestCost = 0
        if (threadCount > items.size) throw Exception("Thread count should be less or equal to number of items")
        compareToBestList(items)
        val threads = ArrayList<Thread>(threadCount)
        var i = 0
        val thItems = ArrayList<Element>(items)
        for (threadIndex in (threadCount - 1) until 0) {
            thItems.removeAt(i++)

            threads[threadIndex] = object: Thread() {
                override fun run() {
                    chooseBestCombo(thItems)
                }
            }
        }
        try {
            threads.forEach {
                it.start()
                if(it.isAlive) it.join()
            }
        }catch (e: InterruptedException){
            e.printStackTrace()
        }
    }

    /**
     * Prints found solving for the knapsack 0/1 problem
     */
    fun printBestItems() = bestItems.forEach { println("${it.name} cost: ${it.cost} weight: ${it.weight}")}
}