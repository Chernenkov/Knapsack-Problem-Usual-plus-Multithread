/**
 * Describes one element in knapsack
 * @param name      name of element
 * @param weight    weight of element
 * @param cost      cost of element
 *
 * @author Alex Chernenkov
 */
data class Element(
        val name: String,
        val weight: Int,
        val cost: Int
)