
fun main(args: Array<String>){
    val items = ArrayList<Element>(7)
    items.add(Element("Shoes", 2, 5000))
    items.add(Element("Notebook", 2, 40000))
    items.add(Element("Jacket", 2, 4000))
    items.add(Element("LunchBox",1, 500))
    items.add(Element("Trousers",4,1500))
    items.add(Element("Apple", 3, 100))
    items.add(Element("Book", 3, 300))
    //items.forEach { println(it.toString()) }
    val knapsack = Knapsack(5)
    var timeBegin = System.currentTimeMillis()
    knapsack.chooseBestCombo(items)
    println("Counted, took ${System.currentTimeMillis() - timeBegin} ms.")
    knapsack.printBestItems()
    //
    timeBegin = System.currentTimeMillis()
    knapsack.threadChooseBestCombo(items, 7) // there are 7 elements in [items]
    println("Counted, took ${System.currentTimeMillis() - timeBegin} ms.")
    knapsack.printBestItems()

}